import React from "react";
import ReactDOM from "react-dom";
import Input from "../input"

 function Btn ({value, className, id, click}){
    return(
        
            <button type="button" className={className} id={id} onClick={()=>{click()}}>{value}</button>
        
    )
} 
export default Btn;