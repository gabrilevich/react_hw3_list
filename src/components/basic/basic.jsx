import {Component} from "react";
import Input from "../input";
import Btn from "../btn"
import "../style/style.css"
import List from "../list/list";

class Basic extends Component{
    
    state = {
        text : []
    }

    clickOk =() =>{ //робимо стрілкову функцію, щоб сюди не пердавався внутрішній контекст, а ті дані, які ми вказуємо
            //ми заховали div в якому має публ. input, щоб його опублікувати ми замінюємо йому клас:шукаємо елемент і видаляємо старий, та даємо новий клас
            let inp = document.querySelector(".inputnone") 
                 inp.classList.remove("inputnone")
                 inp.classList.add("inputOpen")
            }
    
    clickDone = () =>{
        // this.setState({text: [...this.state.text, document.querySelector("#inp").value]}); 
        this.setState((state)=>{
            return {    
                text: [...this.state.text, document.querySelector("#inp").value] //пишемо...this.state.text - щоб не було перезапису, а все йшло по порядку, кожне значення записуємо в масив через деструктиризацію
            }
        });
                               
    }

    clickDelete = ()=>{
        document.querySelector("#inp").value = ""
    }

    deleteText = (index)=>{ //функція для видалення елементу із списку і масиву
        let a = this.state.text
        a.splice(index,1) //в цьому випадку через splice ми видаляємо елемент, index якого прийшов з List і нам повертається змінений масив, який далі ми публікуємо

        this.setState((state)=>{
            return {    
                text: a //пишемо...this.state.text - щоб не було перезапису, а все йшло по порядку, кожне значення записуємо в масив через деструктиризацію
            }          
    })
    } 
    //редагування
    clickEdit = (index) =>{ //функція для видалення елементу із списку і масиву
        let a = this.state.text
        let b = a.splice(index,1)
        document.querySelector("#inp").value = b
       // a.splice(index,1) //в цьому випадку через splice ми видаляємо елемент, index якого прийшов з List і нам повертається змінений масив, який далі ми публікуємо
        this.setState((state)=>{
            return {    
                text: a //пишемо...this.state.text - щоб не було перезапису, а все йшло по порядку, кожне значення записуємо в масив через деструктиризацію
            }          
    }) 
    }

    //куплено - закреслюємо
    clickBought = (id)=>{ //тут при передачі саме className в List чомусь не вsдпрацьовує, але якщо передаю як "list"+index - то все ок
        document.getElementById(id).classList.add("decor")
        console.log (id)
    }

    render () {

        return(
            <>
                <Btn value="додати ➕" id='plus' click={this.clickOk} ></Btn>
                
                
                <div className="inputnone">
                <Input id='inp'></Input>
                <Btn value="зберегти 📀"  id='save' click={this.clickDone}></Btn>
                <Btn value="❌"  id='delet' click={this.clickDelete}></Btn>
                </div>
                
                <List arr = {this.state.text} func={this.deleteText} edit={this.clickEdit} bought={this.clickBought}/>
                                                              
            </>
        )
    }
}


export default Basic;