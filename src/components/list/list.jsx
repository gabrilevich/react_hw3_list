import React from "react";
import Btn from "../btn";



function List({text, click, arr, edit, func, bought}) {
    return(
            <>          
               {arr.map((list, index, className) =>
                <div id = {"list"+index}> 
                <span>{list}</span>
                <Btn value="✍️"  id='edit' click={()=>edit(index)}></Btn>
                <Btn value="❌"  id='delet' click={()=>func(index)}></Btn>
                <Btn value="✅"  id='done'  click={()=>bought("list"+index)}></Btn>
                </div>
                )} 
            
            </>
    )
}

export  default List;

//BTN - це щаблон, в List ми цей шаблон приводимо до потрібного вигляду але оскільки нам самим в List треба прийняти дані з basic, то треба саме вказати що прийняти ы выддати це для List в компоненті basic
//func - на його мысце йде назва потрыбноъ функції і вона запускається
// list - ми не передаємо, оскільки воно буде атвоматично тягнути елементи мсиву, який передали через arr